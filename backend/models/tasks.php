<?php
    $serverName = "localhost";
    $username = "root";
    $password = "";
    $dbname = "finalProject";

    // Create connection
    $dbConnection = new mysqli($serverName, $username, $password, $dbname);
    // Check connection
    if ($dbConnection->connect_error) {
        die("Connection failed: " . $dbConnection->connect_error);
    } 

    // sql to create table
    $taskModel = "CREATE TABLE IF NOT EXISTS Tasks (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        project_id INT(6) UNSIGNED NOT NULL, 
        taskDescription VARCHAR(500) NOT NULL,
        status VARCHAR(30) NOT NULL DEFAULT 'in progress',
        createdAt TIMESTAMP,
        FOREIGN KEY (project_id) REFERENCES Projects(id)
    )";

    if ($dbConnection->query($taskModel) === TRUE) {
        echo "Table Tasks created successfully";
    } else {
        echo "Error creating table: " . $dbConnection->error;
    }

    $dbConnection->close();

?>