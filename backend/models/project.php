<?php
    $serverName = "localhost";
    $username = "root";
    $password = "";
    $dbname = "finalProject";

    // Create connection
    $dbConnection = new mysqli($serverName, $username, $password, $dbname);
    // Check connection
    if ($dbConnection->connect_error) {
        die("Connection failed: " . $dbConnection->connect_error);
    } 

    // sql to create table
    $projectModel = "CREATE TABLE IF NOT EXISTS Projects (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(30) NOT NULL UNIQUE, 
        description VARCHAR(500) NOT NULL,
        status VARCHAR(30) NOT NULL DEFAULT 'progress',
        createdAt TIMESTAMP
    )";

    if ($dbConnection->query($projectModel) === TRUE) {
        echo "Table Projects created successfully";
    } else {
        echo "Error creating table: " . $dbConnection->error;
    }

    $dbConnection->close();

?>