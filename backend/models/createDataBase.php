<?php
$host = "localhost";
$db_name = "finalProject";
$username = "root";
$password = "";
$connection;

    $dbConnection = new mysqli($host, $username);
    // Check connection
    if ($dbConnection->connect_error) {
        die("Connection failed: " . $dbConnection->connect_error);
    }

    // Create database
    $dataBase = "CREATE DATABASE IF NOT EXISTS finalProject CHARACTER SET utf8 COLLATE utf8_general_ci";
    if ($dbConnection->query($dataBase) === TRUE) {
        echo "Database created successfully \n";
    } else {
        echo "Error creating database: " . $dbConnection->error;
    }

    $dbConnection->close();

    $dbConnection = new mysqli($host, $username, $password, $db_name);

    $userModel = "CREATE TABLE IF NOT EXISTS Users (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        username VARCHAR(30) NOT NULL UNIQUE, 
        firstname VARCHAR(30) NOT NULL,
        lastname VARCHAR(30) NOT NULL,
        email VARCHAR(50) NOT NULL UNIQUE,
        password VARCHAR(50) NOT NULL,
        role VARCHAR(50) NOT NULL DEFAULT 'user',
        createdAt TIMESTAMP
    )";
    
    if ($dbConnection->query($userModel) === TRUE) {
        echo "Table Users created successfully\n";
    } else {
        echo "Error creating table: " . $dbConnection->error;
    }

    $projectModel = "CREATE TABLE IF NOT EXISTS Projects (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(30) NOT NULL UNIQUE, 
        description VARCHAR(500) NOT NULL,
        status VARCHAR(30) NOT NULL DEFAULT 'progress',
        createdAt TIMESTAMP
    )";

    if ($dbConnection->query($projectModel) === TRUE) {
        echo "Table Projects created successfully\n";
    } else {
        echo "Error creating table: " . $dbConnection->error;
    }

    $commentModel = "CREATE TABLE IF NOT EXISTS Comments (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        project_id INT(6) UNSIGNED NOT NULL, 
        commentText VARCHAR(500) NOT NULL,
        user VARCHAR(30) NOT NULL,
        createdAt TIMESTAMP,
        FOREIGN KEY (project_id) REFERENCES Projects(id)
    )";

    if ($dbConnection->query($commentModel) === TRUE) {
        echo "Table Comments created successfully\n";
    } else {
        echo "Error creating table: " . $dbConnection->error;
    }

    $taskModel = "CREATE TABLE IF NOT EXISTS Tasks (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        project_id INT(6) UNSIGNED NOT NULL, 
        taskDescription VARCHAR(500) NOT NULL,
        status VARCHAR(30) NOT NULL DEFAULT 'in progress',
        createdAt TIMESTAMP,
        FOREIGN KEY (project_id) REFERENCES Projects(id)
    )";

    if ($dbConnection->query($taskModel) === TRUE) {
        echo "Table Tasks created successfully\n";
    } else {
        echo "Error creating table: " . $dbConnection->error;
    }

    $dbConnection->close();

?>