<?php
    $serverName = "localhost";
    $username = "root";
    $password = "";
    $dbname = "finalProject";

    // Create connection
    $dbConnection = new mysqli($serverName, $username, $password, $dbname);
    // Check connection
    if ($dbConnection->connect_error) {
        die("Connection failed: " . $dbConnection->connect_error);
    } 

    // sql to create table
    $commentModel = "CREATE TABLE IF NOT EXISTS Comments (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        project_id INT(6) UNSIGNED NOT NULL, 
        commentText VARCHAR(500) NOT NULL,
        user VARCHAR(30) NOT NULL,
        createdAt TIMESTAMP,
        FOREIGN KEY (project_id) REFERENCES Projects(id)
    )";

    if ($dbConnection->query($commentModel) === TRUE) {
        echo "Table Comments created successfully";
    } else {
        echo "Error creating table: " . $dbConnection->error;
    }

    $dbConnection->close();

?>