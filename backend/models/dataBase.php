<?php
$host = "localhost";
$db_name = "finalProject";
$username = "root";
$password = "";
$connection;

    $dbConnection = new mysqli($host, $username);
    // Check connection
    if ($dbConnection->connect_error) {
        die("Connection failed: " . $dbConnection->connect_error);
    }

    // Create database
    $dataBase = "CREATE DATABASE IF NOT EXISTS finalProject CHARACTER SET utf8 COLLATE utf8_general_ci";
    if ($dbConnection->query($dataBase) === TRUE) {
        echo "Database created successfully";
    } else {
        echo "Error creating database: " . $dbConnection->error;
    }


?>