<?php

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: GET");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once "../config/database.php";
    include_once "../objects/task.php";

    $database = new Database();
    $db = $database->getConnection();

    $task = new Task($db);

    $task->id = $_POST["id"];
    $task->status = $_POST["status"];

    $task->changeStatus();
    echo json_encode($task, JSON_UNESCAPED_UNICODE);
    

?>