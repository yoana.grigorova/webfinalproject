<?php

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: GET");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once "../config/database.php";
    include_once "../objects/project.php";

    $database = new Database();
    $db = $database->getConnection();

    $project = new Project($db);

    $project->id = $_POST["id"];

    $project->readOne();
    echo json_encode($project, JSON_UNESCAPED_UNICODE);
    

?>