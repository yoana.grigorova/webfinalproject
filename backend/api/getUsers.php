<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: GET");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once "../config/database.php";
    include_once "../objects/user.php";

    $database = new Database();
    $db = $database->getConnection();

    $user = new User($db);
    $statement = $user->read();
    $numberOfResults = $statement->rowCount();
    if($numberOfResults > 0){
        $users = array();
        $users["records"] = array();

        while($row = $statement->fetch(PDO::FETCH_ASSOC)){
            extract($row);

            $userRecord = array(
                "id" => $id,
                "username" => $username,
                "firstName" => $firstName,
                "lastName" => $lastName,
                "email" => $email
            );
            array_push($users["records"], $userRecord);
        }
        http_response_code(200);
        
        echo json_encode($users, JSON_UNESCAPED_UNICODE);
    }
    

?>