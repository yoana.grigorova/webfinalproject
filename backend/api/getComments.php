<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: GET");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once "../config/database.php";
    include_once "../objects/comment.php";

    $database = new Database();
    $db = $database->getConnection();

    $comment = new Comment($db);

    $project_id = $_POST["project_id"];

    $statement = $comment->read($project_id);
    $numberOfResults = $statement->rowCount();
    if($numberOfResults > 0){
        $comments = array();
        $comments["records"] = array();

        while($row = $statement->fetch(PDO::FETCH_ASSOC)){
            extract($row);

            $commentRecord = array(
                "id" => $id,
                "user" => $user,
                "commentText" => $commentText,
                "createdAt" => $createdAt
            );
            array_push($comments["records"], $commentRecord);
        }
        http_response_code(200);
        
        echo json_encode($comments, JSON_UNESCAPED_UNICODE);
    }else{
        http_response_code(503);
        echo json_encode(array("message" => "Няма намерени коментари."), JSON_UNESCAPED_UNICODE);
    }
    

?>