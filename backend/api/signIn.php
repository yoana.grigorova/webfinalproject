<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    include_once "../config/database.php";
    include_once "../objects/user.php";

    session_start();

    $database = new Database();
    $dbConnection = $database->getConnection();

    $user = new User($dbConnection);

    $user->username = $_POST["username"];
    $user->readOne();
        // echo json_encode($user, JSON_UNESCAPED_UNICODE);
    if (sha1($_POST["password"]) == $user->password) {
        // echo "Verified";
        $_SESSION['user_id'] = $user->id;
        echo json_encode($user, JSON_UNESCAPED_UNICODE);
    }else{
        echo "Wrong password";
    }
?>