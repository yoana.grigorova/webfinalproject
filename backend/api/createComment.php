<?php

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once "../config/database.php";
    include_once "../objects/comment.php";

    $database = new Database();
    $db = $database->getConnection();

    $comment = new Comment($db);

    $comment->project_id = $_POST["project_id"];
    $comment->commentText = $_POST["commentText"];
    $comment->user = $_POST["user"];

    if ($comment->create()) {
        http_response_code(201);
        echo json_encode(array("message" => "Коментара беше създаден."), JSON_UNESCAPED_UNICODE);
    } else {
        http_response_code(503);
        echo json_encode(array("message" => "Коментара не беше създаден."), JSON_UNESCAPED_UNICODE);
    }
?>