<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: GET");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once "../config/database.php";
    include_once "../objects/task.php";

    $database = new Database();
    $db = $database->getConnection();

    $task = new Task($db);

    $task->project_id = $_POST["project_id"];

    $statement = $task->read();
    $numberOfResults = $statement->rowCount();
    if($numberOfResults > 0){
        $tasks = array();
        $tasks["records"] = array();

        while($row = $statement->fetch(PDO::FETCH_ASSOC)){
            extract($row);

            $taskRecord = array(
                "id" => $id,
                "status" => $status,
                "taskDescription" => $taskDescription,
                "createdAt" => $createdAt
            );
            array_push($tasks["records"], $taskRecord);
        }
        http_response_code(200);
        
        echo json_encode($tasks, JSON_UNESCAPED_UNICODE);
    }else{
        http_response_code(503);
        echo json_encode(array("message" => "Няма намерени задачи."), JSON_UNESCAPED_UNICODE);
    }
    

?>