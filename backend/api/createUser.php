<?php

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once "../config/database.php";
    include_once "../objects/user.php";

    $database = new Database();
    $db = $database->getConnection();

    $user = new User($db);

    $user->username = $_POST["username"];
    $user->email = $_POST["email"];
    $user->firstName = $_POST["firstName"];
    $user->lastName = $_POST["lastName"] ;
    $user->password = sha1($_POST["password"]);
    // $user->role = $data->role;

    if ($user->create()) {
        http_response_code(201);
        echo json_encode(array("message" => "Потребителят беше създаден."), JSON_UNESCAPED_UNICODE);
    } else {
        http_response_code(503);
        echo json_encode(array("message" => "Имейл адресът е зает."), JSON_UNESCAPED_UNICODE);
    }


?>