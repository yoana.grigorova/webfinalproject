<?php

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once "../config/database.php";
    include_once "../objects/project.php";
    include_once "../objects/user_project.php";

    $database = new Database();
    $db = $database->getConnection();

    $project = new Project($db);
    $user_project = new User_Project($db);

    $project->name = $_POST["name"];
    $project->description = $_POST["description"];
    // $collaborators = json_decode($_POST['collaborators']);

    if ($project->create()) {
        http_response_code(201);
        echo json_encode(array("message" => "Проекта беше създаден."), JSON_UNESCAPED_UNICODE);
    } else {
        http_response_code(503);
        echo json_encode(array("message" => "Името е заето."), JSON_UNESCAPED_UNICODE);
    }

    // $project->readOneByName();

    // $user_project->project_id = $project->id;

    // echo $project->id;
    // foreach($collaborators as $collaborator){
    //     $user_project->user_id = $collaborator;
    //     echo $collaborator;
    //     if($user_project->create()){
    //         http_response_code(201);
    //         echo json_encode(array("message" => "Връзката беше създаден."), JSON_UNESCAPED_UNICODE);
    //     } 
    //     // else {
    //     //     http_response_code(503);
    //     //     echo json_encode(array("message" => "Двойката вече съществува."), JSON_UNESCAPED_UNICODE);
    //     // }
    // }
?>