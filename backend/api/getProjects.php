<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: GET");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once "../config/database.php";
    include_once "../objects/project.php";

    $database = new Database();
    $db = $database->getConnection();

    $project = new Project($db);
    $statement = $project->read();
    $numberOfResults = $statement->rowCount();
    if($numberOfResults > 0){
        $projects = array();
        $projects["records"] = array();

        while($row = $statement->fetch(PDO::FETCH_ASSOC)){
            extract($row);

            $userRecord = array(
                "id" => $id,
                "name" => $name,
                "description" => $description,
                "status" => $status
            );
            array_push($projects["records"], $userRecord);
        }
        http_response_code(200);
        
        echo json_encode($projects, JSON_UNESCAPED_UNICODE);
    }
    

?>