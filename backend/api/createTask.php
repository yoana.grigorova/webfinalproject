<?php

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once "../config/database.php";
    include_once "../objects/task.php";

    $database = new Database();
    $db = $database->getConnection();

    $comment = new Task($db);

    $comment->project_id = $_POST["project_id"];
    $comment->taskDescription = $_POST["taskDescription"];

    if ($comment->create()) {
        http_response_code(201);
        echo json_encode(array("message" => "Задачата беше създаден."), JSON_UNESCAPED_UNICODE);
    } else {
        http_response_code(503);
        echo json_encode(array("message" => "Задачата не беше създаден."), JSON_UNESCAPED_UNICODE);
    }
?>