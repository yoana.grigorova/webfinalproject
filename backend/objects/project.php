<?php
    class Project {

        private $connection;
        private $table_name = "projects";


        public $id;
        public $name;
        public $description;

        public function __construct($db)
        {
            $this->connection = $db;
            
        }

        function create()
        {
            $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    name=:name, description=:description";

            $statement = $this->connection->prepare($query);

            $this->name = htmlspecialchars(strip_tags($this->name));
            $this->description = htmlspecialchars(strip_tags($this->description));

            $statement->bindParam(":name", $this->name);
            $statement->bindParam(":description", $this->description);

            if ($statement->execute()) {
                return true;
            }

            return false;

        }

        function readOne()
        {
            $query = "SELECT
                    id, name, description, status 
                FROM
                    " . $this->table_name . "
                WHERE
                    id = ?";

            $statement = $this->connection->prepare($query);

            $statement->bindParam(1, $this->id);

            $statement->execute();

            $row = $statement->fetch(PDO::FETCH_ASSOC);

            $this->id = $row["id"];
            $this->name = $row["name"];
            $this->description = $row["description"];
            $this->status = $row["status"];
        }

        function readOneByName()
        {
            $query = "SELECT
                    id, name, description, status 
                FROM
                    " . $this->table_name . "
                WHERE
                    name = ?";

            $statement = $this->connection->prepare($query);

            $statement->bindParam(1, $this->name);

            $statement->execute();

            $row = $statement->fetch(PDO::FETCH_ASSOC);

            $this->id = $row["id"];
            $this->name = $row["name"];
            $this->description = $row["description"];
            $this->status = $row["status"];
        }

        function read()
        {
            $query = "SELECT
                    id, name, description, status
                FROM
                " . $this->table_name;

            $statement = $this->connection->prepare($query);

            $statement->execute();

            return $statement;
        }

        function changeStatus(){
            $query = "UPDATE 
                " . $this->table_name . "
                SET status= ?
                WHERE
                 id = ?";


            $statement = $this->connection->prepare($query);

            $statement->bindParam(1, $this->status);
            $statement->bindParam(2, $this->id);
         
            $statement->execute();
        }

    }



?>