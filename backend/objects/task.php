<?php
    class Task {

        private $connection;
        private $table_name = "tasks";


        public $id;
        public $project_id;
        public $taskDescription;
        public $status;

        public function __construct($db)
        {
            $this->connection = $db;
            
        }

        function create()
        {
            $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                project_id=:project_id, taskDescription=:taskDescription";

            $statement = $this->connection->prepare($query);

            $this->project_id = htmlspecialchars(strip_tags($this->project_id));
            $this->taskDescription = htmlspecialchars(strip_tags($this->taskDescription));

            $statement->bindParam(":project_id", $this->project_id);
            $statement->bindParam(":taskDescription", $this->taskDescription);

            if ($statement->execute()) {
                return true;
            }

            return false;

        }

        function read()
        {
            $query = "SELECT
                    id, project_id, taskDescription, status, createdAt
                FROM
                " . $this->table_name. "
                WHERE
                    project_id = ?";

            $statement = $this->connection->prepare($query);

            $statement->bindParam(1, $this->project_id);

            $statement->execute();

            return $statement;
        }

        function changeStatus(){
            $query = "UPDATE 
                " . $this->table_name . "
                SET status= ?
                WHERE
                 id = ?";


            $statement = $this->connection->prepare($query);

            $statement->bindParam(1, $this->status);
            $statement->bindParam(2, $this->id);
         
            $statement->execute();
        }

    }



?>