<?php
    class Comment {

        private $connection;
        private $table_name = "comments";


        public $id;
        public $project_id;
        public $commentText;
        public $user;

        public function __construct($db)
        {
            $this->connection = $db;
            
        }

        function create()
        {
            $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                project_id=:project_id, commentText=:commentText, user=:user";

            $statement = $this->connection->prepare($query);

            $this->project_id = htmlspecialchars(strip_tags($this->project_id));
            $this->commentText = htmlspecialchars(strip_tags($this->commentText));
            $this->user = htmlspecialchars(strip_tags($this->user));

            $statement->bindParam(":project_id", $this->project_id);
            $statement->bindParam(":commentText", $this->commentText);
            $statement->bindParam(":user", $this->user);

            if ($statement->execute()) {
                return true;
            }

            return false;

        }

        function read($project_id)
        {
            $query = "SELECT
                    id, project_id, commentText, user, createdAt
                FROM
                " . $this->table_name. "
                WHERE
                    project_id = ?";

            $statement = $this->connection->prepare($query);

            $statement->bindParam(1, $project_id);

            $statement->execute();

            return $statement;
        }

    }



?>