<?php
    class User_Project{

        private $connection;
        private $table_name = "user_project";


        public $user_id;
        public $project_id;

        public function __construct($db)
        {
            $this->connection = $db;
        }

        function create()
        {
            $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                project_id=:project_id, user_id=:user_id";

            $statement = $this->connection->prepare($query);

            $this->user_id = htmlspecialchars(strip_tags($this->user_id));
            $this->project_id = htmlspecialchars(strip_tags($this->project_id));

            $statement->bindParam(":user_id", $this->user_id);
            $statement->bindParam(":project_id", $this->project_id);

            if ($statement->execute()) {
                return true;
            }

            return false;

        }

        function read()
        {
            $query = "SELECT
                    project_id, user_id
                FROM
                " . $this->table_name;

            $statement = $this->connection->prepare($query);

            $statement->execute();

            return $statement;
        }

        function getUserProjects(){
            $query = "SELECT
                    project_id, user_id
                FROM
                " . $this->table_name . "
                WHERE
                 user_id=?
                ";

            $statement = $this->connection->prepare($query);

            $statement->bindParam(1, $this->user_id);

            $statement->execute();

            return $statement;
        }

        function getProjectUsers(){
            $query = "SELECT
                    project_id, user_id
                FROM
                " . $this->table_name . "
                WHERE
                project_id=?
                ";

            $statement = $this->connection->prepare($query);

            $statement->bindParam(1, $this->project_id);

            $statement->execute();

            return $statement;
        }

    }


?>