document.addEventListener("DOMContentLoaded", function(){

    function sendLoginData(username, password){
        return new Promise(function(resolve, reject){
            var xhr = new XMLHttpRequest();
            xhr.open("POST", 'http://localhost/webfinalproject/backend/api/signIn.php?', true);
    
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    
            xhr.onreadystatechange = function () {
                if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                    resolve(JSON.parse(xhr.responseText));
                }
            }
          
            xhr.send(`username=${username}&password=${password}`);
        })
    }

    document.getElementById("submitLogin").addEventListener("click", function(e){
        e.preventDefault();
        let username = document.getElementById("username").value;
        let password = document.getElementById("password").value;

        sendLoginData(username, password).then(function(user){
            console.log(user);
            let loggedUser = {
                username: user.username,
                email: user.email,
                name: user.firstName + " " + user.lastName
            }

            localStorage.setItem("User", JSON.stringify(loggedUser));

            window.location = "http://localhost/webfinalproject/frontend/projects/list/index.html";
        })
    })
})