document.addEventListener("DOMContentLoaded", function () {

    function getElementValue(id) {
        let value = document.getElementById(id).value;
        return value;
    }

    function toggleVisibility(id, state) {
        document.getElementById(id).style.display = state;
    }

    function validateForm(form) {

        let isValid = true;
        let validEmail = new RegExp("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$");
        let validPassword = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})");

        if (!form.username.length || !form.firstName.length || !form.lastName.length) {
            isValid = false;
        }

        if (!validEmail.test(form.email)) {
            toggleVisibility("emailError", "block");
            isValid = false;
        } else {
            toggleVisibility("emailError", "none");
        }

        if (!validPassword.test(form.password)) {
            toggleVisibility("passwordError", "block");
            isValid = false;
        } else {
            toggleVisibility("passwordError", "none");
        }

        if (form.password !== form.repeatPassword) {
            toggleVisibility("matchError", "block");
            isValid = false;
        } else {
            toggleVisibility("matchError", "none");
        }

        return isValid;

    }

    function sendRegistration(form) {
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open("POST", '../../../backend/api/createUser.php?', true);
    
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    
            xhr.onreadystatechange = function () {
                if (xhr.readyState === XMLHttpRequest.DONE && xhr.status >= 200 && xhr.status < 300) {
                    resolve(xhr.responseText);
                }
            }
          
            xhr.send(`username=${form.username}&firstName=${form.firstName}&lastName=${form.lastName}&email=${form.email}&password=${form.password}`);
            // xhr.send(form);
        })
    }

    document.getElementById("submitRegistration").addEventListener("click", function (e) {
        e.preventDefault();

        let form = {
            username: getElementValue("username"),
            firstName: getElementValue("firstName"),
            lastName: getElementValue("lastName"),
            email: getElementValue("email"),
            password: getElementValue("password"),
            repeatPassword: getElementValue("repeat-password")
        }

        if (validateForm(form)) {
            sendRegistration(form).then(function(data){
                console.log(data);
                window.location = "http://localhost/webfinalproject/frontend/projects/list/index.html";
            })
        }

    })
})