document.addEventListener("DOMContentLoaded", function(){

    if(!localStorage.getItem("User")){
        window.location = "http://localhost/webfinalproject/frontend/auth/signin/index.html"
    }

    function getUsers() {
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open("GET", '../../../backend/api/getUsers.php', true);
    
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    
            xhr.onreadystatechange = function (data) {
                if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                    resolve(xhr.responseText);
                }
            }
          
             xhr.send();
        })
    }

    // getUsers().then(function(users){
    //     users = JSON.parse(users).records;
    //     users.forEach(function(user,index){
    //         let checkbox = document.createElement("input");
    //         let label = document.createElement("label");
    //         let article = document.createElement("article");
    //         checkbox.type = "checkbox";
    //         checkbox.value = user.id;
    //         checkbox.name = user.username;
    //         checkbox.classList.add("collaborator");
    //         label.for = user.username;
    //         label.innerText = user.username;
    //         label.style.display = "inline-block"; 
    //         article.appendChild(checkbox);
    //         article.appendChild(label);
    //         article.style.maxHeight = "200px";
    //         article.style.overflowY = "auto";
    //         let checkboxParent = document.getElementById("colaborators");
    //         checkboxParent.appendChild(article);
    //     })
    // })

    function sendProject(data){
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open("POST", '../../../backend/api/createProject.php?', true);
    
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    
            xhr.onreadystatechange = function () {
                if (xhr.readyState === XMLHttpRequest.DONE && xhr.status >= 200 && xhr.status <300) {
                    resolve(xhr.responseText);
                }
            }
          
            xhr.send(`name=${data.name}&description=${data.description}`);
        })
    }

    document.getElementById("submitProject").addEventListener("click", function(e){
        e.preventDefault();

        let name = document.getElementById("projectName").value;
        let description = document.getElementById("projectDescription").value;

        // let collaborators = [...document.querySelectorAll(".collaborator")];
        // collaborators = collaborators.filter(user => user.checked).map(user => parseInt(user.value));

        let data = {
            name: name,
            description: description,
            // collaborators: JSON.stringify(collaborators)
        }

        if(name.length && description.length){
            sendProject(data).then(function(response){
                console.log(response);
                window.location = "http://localhost/webfinalproject/frontend/projects/list/index.html";
            })
        }
    
    })
})