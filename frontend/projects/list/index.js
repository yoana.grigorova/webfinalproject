document.addEventListener("DOMContentLoaded", function(){

    if(!localStorage.getItem("User")){
        window.location = "http://localhost/webfinalproject/frontend/auth/signin/index.html"
    }

    function getProjects() {
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open("GET", '../../../backend/api/getProjects.php', true);
    
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    
            xhr.onreadystatechange = function (data) {
                if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                    resolve(JSON.parse(xhr.responseText).records);
                }
            }
          
             xhr.send();
        })
    }


    getProjects().then(function(data){
        data.forEach(function(project){
            let row = document.createElement("tr");
            let name = document.createElement("td");
            name.innerText = project.name;

            let link = document.createElement("a");
            link.href="project/index.html#" + project.id;
            link.appendChild(name);
        
            let description = document.createElement("td");
            description.innerText = project.description;

            let status = document.createElement("td");
            let select = document.createElement("select");
            select.classList.add("changeStatus");
            select.name = project.id;
            ["progress", "done"].forEach(function(stat){
                let option = document.createElement("option");
                option.innerText = stat;
                if(project.status == stat){
                    option.selected = "selected";
                }

                select.appendChild(option);
            })
            status.appendChild(select);

            row.appendChild(link);
            row.appendChild(description);
            row.appendChild(status);
            document.getElementById("projects").appendChild(row);

        })

        let selects = [...document.querySelectorAll(".changeStatus")];
        selects.forEach(function(select){
            select.addEventListener("change", function(event){
                changeStatus(event);
            })
        })
    })


   function sendStatusUpdate(project_id, status){
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", '../../../backend/api/changeProjectStatus.php?', true);

        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                resolve(JSON.parse(xhr.responseText).records);
            }
        }
      
         xhr.send(`id=${project_id}&status=${status}`);
    })
   }

    function changeStatus(event){
        sendStatusUpdate(event.target.name, event.target.value).then(function(status){
            console.log(status);
        })
    }
})