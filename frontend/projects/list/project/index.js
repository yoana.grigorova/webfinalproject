document.addEventListener("DOMContentLoaded", function () {
    //Render page 

    function getProject(id) {
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open("POST", '../../../../backend/api/getProject.php?', true);

            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            xhr.onreadystatechange = function () {
                if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                    resolve(xhr.responseText);
                }
            }

            xhr.send(`id=${id}`);
        })
    }

    getProject(window.location.hash.slice(1)).then(function (response) {
        let project = JSON.parse(response);
        console.log(project);
        createPage(project);
    });

    function createPage(project) {
        let title = document.createElement("h1");
        title.innerText = project.name;
        document.querySelector(".title").appendChild(title);

        let description = document.createElement("p");
        description.innerText = project.description;
        document.querySelector(".description").appendChild(description);
    }

    //Comments actions

    function createComment(project_id, username, commentText) {
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open("POST", '../../../../backend/api/createComment.php?', true);

            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            xhr.onreadystatechange = function () {
                if (xhr.readyState === XMLHttpRequest.DONE && xhr.status >= 200 && xhr.status <= 299) {
                    resolve(xhr.responseText);
                }
            }

            xhr.send(`project_id=${project_id}&user=${username}&commentText=${commentText}`);
        })
    }

    function getComments(project_id) {
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open("POST", '../../../../backend/api/getComments.php', true);

            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            xhr.onreadystatechange = function () {
                if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                    resolve(JSON.parse(xhr.responseText).records);
                }
            }

            xhr.send(`project_id=${project_id}`);
        })
    }

    document.getElementById("addComment").addEventListener("click", function () {
        let commentText = document.getElementById("comment_body").value;
        let project_id = window.location.hash.slice(1);
        let username = JSON.parse(window.localStorage.getItem("User")).username;
        console.log(commentText, project_id, username);
        if (commentText.length) {
            createComment(project_id, username, commentText).then(function (comment) {
                console.log(comment);
                renderComments();
            })
        }
    })

    function renderComments() {
        let project_id = window.location.hash.slice(1);
        let comments_container = document.getElementById("comments");
        comments_container.innerHTML = null;
        getComments(project_id).then(function (comments) {
            let table = document.createElement("table");
            table.classList.add("comments_table");
            comments.forEach(function (comment) {
                let head_row = document.createElement("tr");
                head_row.classList.add("comment_header");
                let username = document.createElement("td");
                username.innerText = comment.user;
                let date = document.createElement("td");
                date.innerText = comment.createdAt;
                head_row.appendChild(username);
                head_row.appendChild(date);

                let content_row = document.createElement("tr");
                let commentText = document.createElement("td");
                commentText.innerText = comment.commentText;
                // commentText.colSpan = 2;
                content_row.appendChild(commentText);

                table.appendChild(head_row);
                table.appendChild(content_row);

            })

            comments_container.appendChild(table);
        })
    }

    renderComments();

    //Tab actions

    document.getElementById("loadComments").addEventListener("click", function (event) {
        this.classList.add("active");
        document.getElementById("loadTasks").classList.remove("active");
        document.getElementById("commentsSection").style.display = "block";
        document.getElementById("tasksSection").style.display = "none";
        renderComments();
    })

    document.getElementById("loadTasks").addEventListener("click", function (event) {
        this.classList.add("active");
        document.getElementById("loadComments").classList.remove("active");
        document.getElementById("commentsSection").style.display = "none";
        document.getElementById("tasksSection").style.display = "block";
        renderTasks();
    })


    //Tasks actions

    function createTask(project_id, taskDescription) {
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open("POST", '../../../../backend/api/createTask.php?', true);

            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            xhr.onreadystatechange = function () {
                if (xhr.readyState === XMLHttpRequest.DONE && xhr.status >= 200 && xhr.status <= 299) {
                    resolve(xhr.responseText);
                }
            }

            xhr.send(`project_id=${project_id}&taskDescription=${taskDescription}`);
        })
    }

    function getTasks(project_id) {
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open("POST", '../../../../backend/api/getTasks.php', true);

            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            xhr.onreadystatechange = function () {
                if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                    resolve(JSON.parse(xhr.responseText).records);
                }
            }

            xhr.send(`project_id=${project_id}`);
        })
    }

    document.getElementById("addTask").addEventListener("click", function () {
        let taskDescription = document.getElementById("task_body").value;
        let project_id = window.location.hash.slice(1);
        if (taskDescription.length) {
            createTask(project_id, taskDescription).then(function (task) {
                console.log(task);
                renderTasks();
            })
        }
    })


    function renderTasks() {
        let project_id = window.location.hash.slice(1);
        let tasks_container = document.getElementById("tasks");
        tasks_container.innerHTML = null;
        getTasks(project_id).then(function (tasks) {
            let table = document.createElement("table");
            table.classList.add("comments_table");
            tasks.forEach(function (task) {
                let head_row = document.createElement("tr");
                head_row.classList.add("comment_header");
                let statusTitle = document.createElement("td");
                statusTitle.innerText = "Status";
                let date = document.createElement("td");
                date.innerText = task.createdAt;

                head_row.appendChild(date);
                head_row.appendChild(statusTitle);

                let content_row = document.createElement("tr");
                let taskDescription = document.createElement("td");
                let status = document.createElement("td");
                let select = document.createElement("select");
                select.classList.add("changeStatus");
                select.name = task.id;
                ["in progress", "on hold", "done"].forEach(function (stat) {
                    let option = document.createElement("option");
                    option.innerText = stat;
                    if (task.status == stat) {
                        option.selected = "selected";
                    }

                    select.appendChild(option);
                })
                status.appendChild(select);
                taskDescription.innerText = task.taskDescription;
                // status.innerText = task.status;
                content_row.appendChild(taskDescription);
                content_row.appendChild(status);

                table.appendChild(head_row);
                table.appendChild(content_row);

            })

            tasks_container.appendChild(table);

            let selects = [...document.querySelectorAll(".changeStatus")];
            selects.forEach(function (select) {
                select.addEventListener("change", function (event) {
                    changeStatus(event);
                })
            })
        })

    }

    function sendStatusUpdate(project_id, status) {
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open("POST", '../../../../backend/api/changeTaskStatus.php?', true);

            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            xhr.onreadystatechange = function () {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    resolve(JSON.parse(xhr.responseText).records);
                }
            }

            xhr.send(`id=${project_id}&status=${status}`);
        })
    }

    function changeStatus(event) {
        sendStatusUpdate(event.target.name, event.target.value).then(function (status) {
            console.log(status);
        })
    }
})